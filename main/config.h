#ifndef __CONFIG__H__
#define __CONFIG__H__

#include <stdio.h>
#include <stdlib.h>

#define WAIT_SEMAPHORE_TIME 2000
#define TX_INTERVAL 10000
#define LMIC_LOOP_TASK_PRIORITY 5
#define TX_TASK_PRIORITY 8

// dev eui 65 79 fe b0 a6 4a 51 b2

// LoRaWAN NwkSKey, network session key
static const unsigned char NWKSKEY[16] = {0x6d, 0xef, 0xf2, 0x83, 0x71, 0x34,
                                          0xa7, 0xcb, 0x05, 0x1c, 0x9b, 0x5f,
                                          0xd5, 0x70, 0xfd, 0xc2};

// LoRaWAN AppSKey, application session key
static const unsigned char APPSKEY[16] = {0x2b, 0x06, 0x80, 0x40, 0x81, 0x41,
                                          0x22, 0xcc, 0x16, 0x7e, 0x20, 0x27,
                                          0xdc, 0x62, 0x16, 0x7f};
// LoRaWAN end-device address (DevAddr)
static const uint32_t DEVADDR =
    0x019576b1; // <-- Change this address for every node!

#endif //!__CONFIG__H__