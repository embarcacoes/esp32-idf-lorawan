#include "config.h"
#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/projdefs.h"
#include "freertos/queue.h"
#include "freertos/semphr.h"
#include "freertos/task.h"
#include <lmic.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static const char *TAG = "ESP_NODE";

const lmic_pinmap lmic_pins = {
    .nss = 18, // CS
    .rxtx = LMIC_UNUSED_PIN,
    .rst = 14,           // RESET
    .dio = {26, 35, 32}, // DIO 0, DIO 1, DIO 2
    .spi = {19, 27, 5},  // MISO, MOSI, CLK
};

SemaphoreHandle_t tx_semphore;

void onEvent(ev_t ev);
static void tx_send(unsigned char *dados);
void txTask(void *pvParameters);
void LMIC_Loop_Task(void *pvParameters);

void os_getArtEui(u1_t *buf) {}
void os_getDevEui(u1_t *buf) {}
void os_getDevKey(u1_t *buf) {}

void app_main() {

    ESP_LOGI(TAG, "Inicando Sistema");

    ESP_LOGI(TAG, "Criando semaforo");
    tx_semphore = xSemaphoreCreateBinary();

    os_init();

    vTaskDelay(pdMS_TO_TICKS(30));

    LMIC_reset();

    ESP_LOGI(TAG, "Configurando sessão LoRaWAN");
    LMIC_setSession(0x1, DEVADDR, (unsigned char *)NWKSKEY,
                    (unsigned char *)APPSKEY);

    ESP_LOGI(TAG, "Escolhendo banda");
    LMIC_selectSubBand(7);

    // Disable link check validation
    LMIC_setLinkCheckMode(0);

    // TTN uses SF9 for its RX2 window.
    LMIC.dn2Dr = DR_SF7;

    // Set data rate and transmit power for uplink (note: txpow seems to be
    // ignored by the library)
    ESP_LOGI(TAG, "Configurando TX Power");
    LMIC_setDrTxpow(DR_SF7, 2);

    ESP_LOGI(TAG, "Iniciando joining");

    LMIC_startJoining();

    ESP_LOGI(TAG, "Criando Tasks");

    xTaskCreate(txTask, "TXTASK", 4096, NULL, TX_TASK_PRIORITY, NULL);
    xTaskCreatePinnedToCore(LMIC_Loop_Task, "LMIC_LOOP", 4096, NULL,
                            LMIC_LOOP_TASK_PRIORITY, NULL, 1);
}

void txTask(void *pvParameters) {
    ESP_LOGI(TAG, "Iniciando Task de envio de dados");

    unsigned char buffer[48];

    memset(buffer, 0, 47);

    int counter = 0;

    xSemaphoreGive(tx_semphore);

    while (1) {
        if (xSemaphoreTake(tx_semphore, pdMS_TO_TICKS(WAIT_SEMAPHORE_TIME)) ==
            true) {
            sprintf((char *)buffer, "{\"counter\":%d}", counter++);

            ESP_LOGI(TAG, "Enviando dados: %s, tamanho: %d", buffer,
                     strlen((char *)buffer));

            tx_send(buffer);
        } else {
            ESP_LOGI(TAG,
                     "Dados ainda não enviados, aguardando fila ser liberada");

            vTaskDelay(pdMS_TO_TICKS(100));
        }

        vTaskDelay(pdMS_TO_TICKS(TX_INTERVAL));
    }

    vTaskDelete(NULL);
}

void LMIC_Loop_Task(void *pvParameters) {
    ESP_LOGI(TAG, "Iniciando Task de loop LMIC");

    while (1) {
        os_runloop_once();

        vTaskDelay(1);
    }

    vTaskDelete(NULL);
}

void onEvent(ev_t ev) {
    switch (ev) {
    case EV_SCAN_TIMEOUT:
        ESP_LOGI(TAG, "EV_SCAN_TIMEOUT");
        break;
    case EV_BEACON_FOUND:
        ESP_LOGI(TAG, "EV_BEACON_FOUND");
        break;
    case EV_BEACON_MISSED:
        ESP_LOGI(TAG, "EV_BEACON_MISSED");
        break;
    case EV_BEACON_TRACKED:
        ESP_LOGI(TAG, "EV_BEACON_TRACKED");
        break;
    case EV_JOINING:
        ESP_LOGI(TAG, "EV_JOINING");
        break;
    case EV_JOINED:
        ESP_LOGI(TAG, "EV_JOINED");
        break;
    case EV_RFU1:
        ESP_LOGI(TAG, "EV_RFU1");
        break;
    case EV_JOIN_FAILED:
        ESP_LOGI(TAG, "EV_JOIN_FAILED");
        break;
    case EV_REJOIN_FAILED:
        ESP_LOGI(TAG, "EV_REJOIN_FAILED");
        break;
    case EV_TXCOMPLETE:
        ESP_LOGI(TAG, "EV_TXCOMPLETE (includes waiting for RX windows)");
        if (LMIC.txrxFlags & TXRX_ACK)
            ESP_LOGI(TAG, "Received ack");
        if (LMIC.dataLen)
            ESP_LOGI(TAG, "Received %d bytes of payload", LMIC.dataLen);

        xSemaphoreGive(tx_semphore);
        break;
    case EV_LOST_TSYNC:
        ESP_LOGI(TAG, "EV_LOST_TSYNC");
        break;
    case EV_RESET:
        ESP_LOGI(TAG, "EV_RESET");
        break;
    case EV_RXCOMPLETE:
        // data received in ping slot
        ESP_LOGI(TAG, "EV_RXCOMPLETE");
        break;
    case EV_LINK_DEAD:
        ESP_LOGI(TAG, "EV_LINK_DEAD");
        break;
    case EV_LINK_ALIVE:
        ESP_LOGI(TAG, "EV_LINK_ALIVE");
        break;
    default:
        ESP_LOGI(TAG, "Unknown event");
        break;
    }
}

static void tx_send(unsigned char *dados) {
    if (LMIC.opmode & OP_TXRXPEND) {
        ESP_LOGI(TAG, "OP_TXRXPEND, not sending");
    } else {
        // Prepare upstream data transmission at the next possible time.
        LMIC_setTxData2(1, dados, strlen((char *)dados), 0);
        ESP_LOGI(TAG, "Packet queued");
        LMIC_sendAlive();
    }
}